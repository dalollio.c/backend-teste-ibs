package com.dalollio.restapi.dao;


import org.springframework.stereotype.Repository;
import com.dalollio.restapi.domain.Pessoa;

@Repository
public class PessoaDaoImpl extends AbstractDao<Pessoa, Long> implements PessoaDao {

}
