package com.dalollio.restapi.dao;

import java.util.List;
import com.dalollio.restapi.domain.Pessoa;

public interface PessoaDao {
	
	void save(Pessoa pessoa);
	
	void update(Pessoa pessoa);
	
	void delete(Long id);
	
	Pessoa findById(Long ir);
	
	List<Pessoa> findAll();

}
