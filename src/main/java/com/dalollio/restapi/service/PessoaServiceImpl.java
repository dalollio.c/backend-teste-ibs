package com.dalollio.restapi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dalollio.restapi.dao.PessoaDao;
import com.dalollio.restapi.domain.Pessoa;


@Service
@Transactional(readOnly = true)
public class PessoaServiceImpl implements PessoaService {

	@Autowired // Permite acessar os métodos das entidades, possibilitando acesso ao banco
	private PessoaDao dao;
	
	@Transactional(readOnly = false) // Diz que o método poderá salvar no banco
	@Override
	public void salvar(Pessoa pessoa) {
		dao.save(pessoa);
		
	}

	@Transactional(readOnly = false)
	@Override
	public void editar(Pessoa pessoa) {
		dao.update(pessoa);
		
	}

	@Override
	@Transactional(readOnly = false)
	public void excluir(Long id) {
		dao.delete(id);
		
	}

	@Override
	public Pessoa buscarPorId(Long id) {
		return dao.findById(id);
	}

	@Override
	public List<Pessoa> buscarTodos() {
		return dao.findAll();
	}

}
