/*
 * @Autor: Carlos Dalollio
 * THE CONTROLLER PROVIDES AN API INTERFACE, WITH ENDPOINTS TO ACCESS  
 */

package com.dalollio.restapi.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.dalollio.restapi.domain.Pessoa;
import com.dalollio.restapi.service.PessoaService;

@RestController
@RequestMapping("/")
public class ApiController {
	
	@Autowired
	PessoaService service;
	
	
	// METHOD NOT USED, BUT IMPLEMENTED FOR TESTS AND INDIVIDUAL CONSULTANT
	@RequestMapping(path = "/{id}")
	public ResponseEntity<Pessoa> getResponse(@PathVariable Long id) {
		
	// USE ResponseEntity() TO SEND RESPONSE
	// ResponseEntity RETURN RESPONSE WITH HEADER (BAD_REQUEST, NOT_FOUND, SUCCESS)
		try {
			Pessoa result = service.buscarPorId(id);
			if (result != null) {
				return ResponseEntity.ok(result);
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception arg) {
			return ResponseEntity.badRequest().build();
		}
	}
	
	// RETURN ALL DATA TO FEED OR REFRESH TABLES WITHOUT PERFORMING ANY ACTION
	@RequestMapping //(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Pessoa>> getAllData(){
		try {
			return ResponseEntity.ok(service.buscarTodos());
		} catch (Exception arg ) {
			return ResponseEntity.badRequest().build();
		}
		
	}

	// ADD NEW PERSON AT DB;
	// THE RETURN OF ALL ACTION IS A LIST OF PEOPLE OBJECT (PEOPLE TABLE), FOR FEEDING TABLE
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Pessoa>> postResponse(@Valid @RequestBody Pessoa pessoa) {
		try {
			/*
			System.out.println(pessoa.getNome());
			System.out.println(pessoa.getCpf());
			System.out.println(pessoa.getEmail());
			System.out.println(pessoa.getEndereco());
			System.out.println(pessoa.getObs());
			System.out.println(pessoa.getTelefone());
			System.out.println(pessoa.getNascimento());
			List<Pessoa> pessoas = service.buscarTodos();
			for (Pessoa pessoA : pessoas) {
				System.out.println(pessoA.getId());
			}
			*/
			service.salvar(pessoa);
			return ResponseEntity.ok(service.buscarTodos());
		} catch (Exception arg) {
			return ResponseEntity.badRequest().build();
		}
	}
	
	// PUT METHOD RECEIVE A "peopleForm", AND MERGE ON DB
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Pessoa>> putResponse(@RequestBody Pessoa pessoa) {
		try {
			if (service.buscarPorId(pessoa.getId()) != null) {
				service.editar(pessoa);
				return ResponseEntity.ok(service.buscarTodos());
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception arg)	{
			return ResponseEntity.badRequest().build();
		}
	}
	
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<List<Pessoa>> deleteResposne(@PathVariable Long id) {
		System.out.println("deleteResponse()");
		try {
			if (service.buscarPorId(id) != null) {
				service.excluir(id);
				return ResponseEntity.ok(service.buscarTodos());
			} else {
				return ResponseEntity.notFound().build();				
			}
		} catch (Exception arg) {
			return ResponseEntity.badRequest().build();
		}
	}
	
}
