package com.dalollio.restapi;

import javax.sql.DataSource;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Configuration
public class DataSourceConfig {
	
	@Bean
	public DataSource getDataSource() {
		int id = 0;
		// ###### CODE OK ####### 
		if (id == 0) {
			DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
			dataSourceBuilder.driverClassName("com.mysql.jdbc.Driver");
			dataSourceBuilder.url("jdbc:mysql://localhost:3306/restFull?createDatabaseIfNotExist=true");
			dataSourceBuilder.username("root");
			dataSourceBuilder.password("root");
			return dataSourceBuilder.build();
		} else {
			DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
			dataSourceBuilder.driverClassName("com.mysql.jdbc.Driver");
			dataSourceBuilder.url("jdbc:mysql://localhost:3306/restFull2?createDatabaseIfNotExist=true");
			dataSourceBuilder.username("root");
			dataSourceBuilder.password("root");
			return dataSourceBuilder.build();
		}
	}
}
