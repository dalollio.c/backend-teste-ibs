package com.dalollio.restapi.domain;

import java.time.LocalDate;

import javax.persistence.*;

@Entity
@Table(name = "PESSOAS")
public class Pessoa {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/*
	 * Argumentos @Column:
	 * name > nome (!!!!!)
	 * nullable > controla se o valor pode ou não ser null
	 * length > tamanho do campo
	 * columnDefinition > definição do tipo da coluna na DB
	 * unique > restringe valores repetidos na mesma coluna
	 * 
	 */
	
	@Column(nullable = false)
	private String nome;
	
	@Column(nullable = false)	
	private String endereco;
	
	@Column(nullable = false)	
	private String email;
	
	@Column(nullable = false)
	private String telefone;
	
	@Column(nullable = false, unique = true)
	private String cpf;
	
	@Column(nullable = false, columnDefinition = "DATE")
	private LocalDate nascimento;
	
	private String obs;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public LocalDate getNascimento() {
		return nascimento;
	}
	public void setNascimento(LocalDate nascimento) {
		this.nascimento = nascimento;
	}
	
	public String getObs() {
		return obs;
	}
	public void setObs(String obs) {
		this.obs = obs;
	}


}
